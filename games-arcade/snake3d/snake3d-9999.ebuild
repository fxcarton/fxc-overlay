# Copyright 2020-2021 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3

DESCRIPTION="3D snake game"
HOMEPAGE="https://pedantic.software/git/3dsh_demos"
SRC_URI=""
EGIT_REPO_URI="${HOMEPAGE}"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND=""
RDEPEND="media-gfx/3dsh"
BDEPEND=""

src_configure() { :; }

src_compile() { :; }

src_install() {
	newbin snake3d.3dsh snake3d
}
