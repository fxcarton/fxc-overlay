# Copyright 2021 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs

DESCRIPTION="SFNT (TrueType, OpenType) font parsing library"
HOMEPAGE="https://pedantic.software/projects/libsfnt.html"
SRC_URI="https://pedantic.software/git/${PN}/dist/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="static-libs"

DEPEND=""
RDEPEND=""
BDEPEND=""

src_configure() {
	:
}

src_compile() {
	tc-export CC
	emake PREFIX=/usr INCLUDEDIR=include LIBDIR="$(get_libdir)" MANDIR=share/man VERSION="${PV}" \
		LIBTYPE="so$(usex static-libs ' a' '')" \
		libsfnt.so $(usex static-libs libsfnt.a '') libsfnt.pc
}

src_test() {
	:
}

src_install() {
	emake DESTDIR="${ED}" PREFIX=/usr INCLUDEDIR=include LIBDIR="$(get_libdir)" MANDIR=share/man \
		LIBTYPE="so$(usex static-libs ' a' '')" \
		install
}
