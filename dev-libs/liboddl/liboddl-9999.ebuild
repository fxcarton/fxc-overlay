# Copyright 2020-2021 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit multilib toolchain-funcs git-r3

DESCRIPTION="A minimalist C89 parser for the Open Data Description Language"
HOMEPAGE="https://pedantic.software/git/liboddl"
EGIT_REPO_URI="${HOMEPAGE}"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="sys-devel/flex"

src_compile() {
	emake CC="$(tc-getCC)"
}

src_install() {
	emake DESTDIR="${D}" PREFIX="${EPREFIX}/usr" LIBDIR="$(get_libdir)" install
}
