# Copyright 2020-2021 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs git-r3

DESCRIPTION="Small console based menu selector, inspired by dmenu"
HOMEPAGE="https://pedantic.software/projects/choice.html"
SRC_URI=""
EGIT_REPO_URI="https://pedantic.software/git/choice"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND=""
RDEPEND=""
BDEPEND=""

src_configure() {
	:
}

src_compile() {
	emake CC="$(tc-getCC)" PREFIX="${EPREFIX}/usr"
}

src_install() {
	emake DESTDIR="${D}" PREFIX="${EPREFIX}/usr" install
}
