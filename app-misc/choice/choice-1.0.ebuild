# Copyright 2020-2021 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs

DESCRIPTION="Small console based menu selector, inspired by dmenu"
HOMEPAGE="https://pedantic.software/projects/choice.html"
SRC_URI="https://pedantic.software/git/${PN}/dist/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND=""
BDEPEND=""

src_configure() {
	:
}

src_compile() {
	emake CC="$(tc-getCC)" PREFIX="${EPREFIX}/usr"
}

src_install() {
	emake DESTDIR="${D}" PREFIX="${EPREFIX}/usr" install
}
