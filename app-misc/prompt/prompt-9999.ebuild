# Copyright 2020-2021 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs git-r3

DESCRIPTION=""
HOMEPAGE="https://pedantic.software/git/~fxc/prompt"
SRC_URI=""
EGIT_REPO_URI="${HOMEPAGE}"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE="git"

DEPEND=""
RDEPEND="git? ( dev-vcs/git )"
BDEPEND=""

src_compile() {
	emake CC="$(tc-getCC)" PREFIX="${EPREFIX}/usr" GIT="$(usex git 1 '')"
}

src_install() {
	emake DESTDIR="${D}" PREFIX="${EPREFIX}/usr" install
}
