# Copyright 2019-2021 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit multilib toolchain-funcs git-r3

DESCRIPTION="Script interpreter and REPL for game"
HOMEPAGE="https://pedantic.software/git/3dsh"
SRC_URI=""
EGIT_REPO_URI="${HOMEPAGE}"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS=""
IUSE="opengex"

DEPEND="media-libs/3dmr:=[opengex=]"
RDEPEND="${DEPEND}"
BDEPEND="virtual/pkgconfig"

src_compile() {
	emake CC="$(tc-getCC)" PREFIX="${EPREFIX}/usr" LIBDIR="$(get_libdir)"
}

src_install() {
	emake install DESTDIR="${D}" PREFIX="${EPREFIX}/usr" LIBDIR="$(get_libdir)"
}
