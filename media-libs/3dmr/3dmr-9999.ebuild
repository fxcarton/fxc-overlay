# Copyright 2019-2021 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit multilib toolchain-funcs virtualx git-r3

DESCRIPTION="3d rendering library"
HOMEPAGE="https://pedantic.software/git/3dmr"
SRC_URI="test? ( https://pedantic.software/fxc/tux.png.gz -> 3dmr-tux.png.gz )"
EGIT_REPO_URI="${HOMEPAGE}"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE="opengex truetype"

RDEPEND="media-libs/glfw:0=
	media-libs/glew:0=
	media-libs/libpng:0=
	media-libs/libjpeg-turbo:0=
	opengex? ( dev-libs/liboddl )
	truetype? ( dev-libs/libsfnt )"
DEPEND="${RDEPEND} test? ( media-gfx/imagemagick sys-devel/bc )"
BDEPEND="virtual/pkgconfig"

src_unpack() {
	git-r3_src_unpack
	if use test; then
		unpack "3dmr-tux.png.gz"
		mkdir -p "${WORKDIR}/${P}/test/assets" || die
		mv "${WORKDIR}/3dmr-tux.png" "${WORKDIR}/${P}/test/assets/tux.png" || die
	fi
}

tdmr_use() {
	printf '%s=%s\n' "$2" "$(usex "$1" 1 '')"
}

tdmr_args() {
	tdmr_use opengex OPENGEX
	tdmr_use truetype TTF
}

src_compile() {
	emake CC="$(tc-getCC)" PREFIX="${EPREFIX}/usr" $(tdmr_args)
}

src_install() {
	emake DESTDIR="${D}" PREFIX="${EPREFIX}/usr" LIBDIR="$(get_libdir)" $(tdmr_args) install
}

src_test() {
	virtx emake -j1 $(tdmr_args) test
}
