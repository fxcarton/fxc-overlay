# Copyright 2021 François-Xavier Carton
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs git-r3

DESCRIPTION="Simple lib to draw menus with 3dmr"
HOMEPAGE="https://pedantic.software/git/lib3dmenu"
SRC_URI=""
EGIT_REPO_URI="${HOMEPAGE}"
EGIT_BRANCH="fxc/master"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS=""

RDEPEND="media-libs/3dmr[truetype]"
DEPEND="${RDEPEND}"
BDEPEND="virtual/pkgconfig"

src_compile() {
	emake CC="$(tc-getCC)" PREFIX="${EPREFIX}/usr" LIBDIR="$(get_libdir)" \
		lib3dmenu.a lib3dmenu.pc
}

src_install() {
	emake DESTDIR="${ED}" PREFIX="${EPREFIX}/usr" LIBDIR="$(get_libdir)" \
		install
}
